
size = [100, 200, 400, 600, 800, 1000]

import matplotlib.pyplot as plt
import numpy as np


kji_time = [
    [0.001, 0.001, 0.001, 0.001, 0.001],
    [0.009, 0.009, 0.009, 0.008, 0.009],
    [0.059, 0.056, 0.050, 0.060, 0.048],
    [0.169, 0.170, 0.161, 0.165, 0.164],
    [0.412, 0.385, 0.377, 0.399, 0.430],
    [0.835, 0.795, 0.760, 0.738, 0.777]
]

kji_error = [
    [1.5e-4, 1.12e-4, 1.4e-4, 1.5e-4, 4.9e-5],
    [5.5e-4, 7.7e-2, 3.7e-4, 4.5e-4, 5.2e-4],
    [6e-3, 2e-3, 3e-3, 4e-3, 1e-3],
    [6e-3, 2e-3, 3e-3, 6e-3, 0.26],
    [5e-3, 6e-3, 9e-3, 1e-2, 5e-2],
    [2e-2, 6e-2, 0.35, 9e-2, 6e-2]
]

kij_time = [
    [0.001, 0.001, 0.001, 0.001, 0.002],
    [0.016, 0.012, 0.012, 0.010, 0.012],
    [0.082, 0.079, 0.078, 0.080, 0.079],
    [0.260, 0.241, 0.232, 0.237, 0.231],
    [0.615, 0.602, 0.605, 0.586, 0.632],
    [1.357, 1.190, 1.210, 1.175, 1.188]
]

kij_error = [
    [5e-5, 1.12e-4, 7e-5, 6e-4, 9e-5],
    [2.3e-3, 7e-4, 2e-3, 6e-3, 4e-4],
    [2e-3, 3e-3, 7e-3, 2e-3, 2e-2],
    [],
    [],
    []
]


kji = []
kij = []
for i, _ in enumerate(kji_time):
    kji.append(np.mean(kji_time[i]))
    kij.append(np.mean(kij_time[i]))


plt.plot(size, kji, color='tab:red', label='kji')
plt.plot(size, kij, color='tab:green', label='kij')
plt.grid(True)
plt.legend()
plt.xlabel('size of matrix')
plt.ylabel('time, sec')

plt.savefig("benchmark.png")
# plt.show()
