program LU

	implicit none
    
    real, allocatable :: A(:, :), B(:, :), L(:, :), U(:, :), C(:, :)
	real (8) :: start_time
	real (8) :: end_time
	integer i, j, k, n

	n = 1000
	
	allocate (A(1:n, 1:n)); A=0;
	allocate (B(1:n, 1:n)); B=0;
	allocate (L(1:n, 1:n)); L=0;
	allocate (U(1:n, 1:n)); U=0;
	allocate (C(1:n, 1:n)); C=0;
    
    call random_number(A)

    do i = 1, n
        A(i, i) = A(i, i) + 0
    end do

    B=A;

	call CPU_TIME(start_time)

	! LU kji
	do k = 1, n-1
	    A(k+1:n, k) = A(k+1:n, k) / A(k, k)
		do j = k+1, n
			A(k+1:n, j) = A(k+1:n, j) - A(k+1:n, k) * A(k, j)
		end do
    end do

	call CPU_TIME(end_time)

	print '("Computation time: " f6.3, "sec")', (end_time - start_time)

	!Lower triangle part (L matrix)
	do j=1,n
	  L(j,j)=1; L(j+1:n,j)=A(j+1:n,j);
	end do

	!Upper triangle part (U matrix)
	do j = 1, n
	  U(1:j, j) = A(1:j ,j);
	end do

	C = matmul(L, U)

	print*, maxval(abs(B - C))

	deallocate (A)
	deallocate (B)
	deallocate (L)
	deallocate (U)
	deallocate (C)

end program LU
